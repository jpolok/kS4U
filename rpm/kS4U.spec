%{!?dist: %define dist .slc5}
Name: @PACKAGE@
Version: @VERSION@
Release: @RELEASE@%{?dist}
Summary: kS4U - utility to acquire Kerberos credentials for User (S4U)
Group: Applications/System
Source: %{name}-%{version}.tar.gz
License: GPL
Vendor: CERN
Packager: Jaroslaw.Polok@cern.ch
URL: http://cern.ch/linux/

BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
BuildArch: noarch

Requires: perl-Authen-Krb5 >= 1.9-1.el6.cern.1

%description 
%{name} is a 

%prep
%setup -q

%build
make all

%install
mkdir -p $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT/

%clean
rm -rf $RPM_BUILD_ROOT

%files 
%defattr(-,root,root)
%{_bindir}/kS4U
%{_mandir}/man3/kS4U.3*
%doc README

%changelog
* Sun Oct 13 2013 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.1-1
- initial test release.
